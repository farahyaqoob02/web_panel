import 'package:cloud_firestore/cloud_firestore.dart';

class CustomerService {
  Future addCustomer(
      String customerId,
      String name,
      String number,
      String details,
      String address,
      String information,
      String job,
      String salary,
      String lastName) async {
    await FirebaseFirestore.instance.collection("Customers").add({
      "CustomerId": customerId,
      "Name": name,
      "Number": number,
      "Details": details,
      "Address": address,
      "Information": information,
      "Job": job,
      "salary": salary,
      "lastName": lastName
    });
  }

   getAllCustomer() async {
     await FirebaseFirestore.instance
        .collection('Customers').get().then((value){
          print('value');
          print(value.docs);
        });
     
   
  }
}
