import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class CustomExpansionCard2 extends StatelessWidget {
  final icon;
 final VoidCallback onPressed1;
 final VoidCallback onPressed2;
 final VoidCallback onPressed3;
 final String text;
 final String childText1;
 final String childText2;
 final String childText3;
  CustomExpansionCard2(
     this.onPressed1,
      this.onPressed2,
      this.onPressed3,
      {this.icon,
     
      this.text='',
      this.childText1='',
      this.childText2='',
      this.childText3=''});
  @override
  Widget build(BuildContext context) {
    return ExpansionCard(
      margin: EdgeInsets.zero,
      title: Row(
        children: [
          Icon(
            icon,
            color: CustomColor.mainColor,
            size: 17,
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            text,
            style: TextStyle(color: CustomColor.mainColor, fontSize: 15),
          ),
        ],
      ),
      children: <Widget>[
          Container(
          color: CustomColor.drawerColor,
          padding: EdgeInsets.only(left: 15, top: 5, bottom: 5),
          child: Row(
            children: [
              Container(
                  // child: Icon(
                  //   Icons.account_circle_sharp,
                  //   color: CustomColor.textColor,
                  //   size: 20,
                  // ),
                  ),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: onPressed1,
                child: Text(
                  childText1,
                  style: TextStyle(
                    color: CustomColor.textColor,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          color: CustomColor.drawerColor,
          padding: EdgeInsets.only(left: 15, top: 5, bottom: 5),
          child: Row(
            children: [
              Container(
                  // child: Icon(
                  //   Icons.account_circle_sharp,
                  //   color: CustomColor.textColor,
                  //   size: 20,
                  // ),
                  ),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: onPressed2,
                child: Text(
                  childText2,
                  style: TextStyle(
                    color: CustomColor.textColor,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 15, top: 5, bottom: 5),
          color: CustomColor.drawerColor,
          child: Row(
            children: [
              // Icon(
              //   Icons.account_circle_sharp,
              //   color: CustomColor.textColor,
              //   size: 20,
              // ),
              SizedBox(
                width: 5,
              ),
              GestureDetector(
                onTap: onPressed3,
                child: Text(
                  childText3,
                  style: TextStyle(
                    color: CustomColor.textColor,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
