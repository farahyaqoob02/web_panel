import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
class ProductsPage extends StatefulWidget {
   final icon;
  ProductsPage({this.icon});
  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         backgroundColor: CustomColor.mainColor,
      appBar: widget.icon==''?AppBar(
        
        title: Text(
          "Products",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ):AppBar(
        leading: widget.icon,
          backgroundColor: CustomColor.mainColor,
        title:Text(
                  "Products",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ), 
      ),
          body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
            
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Products",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}