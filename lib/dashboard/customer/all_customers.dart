import 'package:bidirectional_scroll_view/bidirectional_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/dashboard/customer/add_customer.dart';
import 'package:web_project/dashboard/customer/edit_customer.dart';
import 'package:web_project/dashboard/customer/view_customer.dart';

class AllCustomers extends StatefulWidget {
  final icon;
  final double width;
  final bool value;
  AllCustomers({this.icon = '', this.width = 0, this.value = false});
  @override
  _AllCustomersState createState() => _AllCustomersState();
}

class _AllCustomersState extends State<AllCustomers> {
  List<Map> customer = [
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "2",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "3",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "4",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "5",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "6",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "7",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "8",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "9",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "10",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
  ];
  List tableTitle = [
    "CustomerId",
    "Name",
    "Number",
    "Details",
    "Address",
    "Information",
    "Job",
    "salary",
    "lastName",
    "Actions",
  ];

  @override
  Widget build(BuildContext context) {
    TableCell getTableTitle(int index) {
      return TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            tableTitle[index],
            style: TextStyle(
                color: CustomColor.drawerColor,
                fontWeight: FontWeight.bold,
                fontSize: 17),
          ),
        ),
      );
    }

    showAlertDialog(BuildContext context, String customerKey) {
      // set up the buttons
      Widget cancelButton = FlatButton(
        child: Text("Cancel"),
        onPressed: () {
          Navigator.pop(context);
        },
      );
      Widget continueButton = FlatButton(
          child: Text(
            "Delete",
            style: TextStyle(color: Colors.red),
          ),
          onPressed: () {
            for (int i = 0; i <= customer.length; i++) {
              if (customer[i]["id"] == customerKey) {
                customer.removeAt(i);

                setState(() {
                  customer = customer;
                });
                break;
              }
            }
            Navigator.pop(context);
          });

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Delete Customer",
            style: TextStyle(color: CustomColor.drawerColor)),
        content: Text("Are you sure, you want to delete this customer?"),
        actions: [
          cancelButton,
          continueButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    TableRow getTableChildren(int index) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                customer[index]["id"],
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["name"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["number"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["details"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["address"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["information"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["job"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["salary"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              customer[index]["lastName"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.edit,
                      color: Colors.green[400],
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditCustomers(
                                    tableTitle,
                                    value: widget.value,
                                  )));
                    }),
                IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      setState(() {
                        showAlertDialog(context, customer[index]["id"]);
                      });
                    }),
                IconButton(
                    icon: Icon(
                      Icons.visibility,
                      color: CustomColor.drawerColor,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ViewCustomer()));
                    })
              ],
            ),
          ),
        ),
      ]);
    }

   
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: widget.icon == ''
            ? AppBar(
                actions: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlatButton.icon(
                        hoverColor: Colors.grey[400],
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddCustomer(
                                        tableTitle,
                                        value: widget.value,
                                      )));
                        },
                        icon: Icon(
                          Icons.add,
                          color: CustomColor.drawerColor,
                        ),
                        label: Text(
                          "Add",
                          style: TextStyle(
                              color: CustomColor.drawerColor,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )),
                  )
                ],
                title: Text(
                  "All Customers",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
                backgroundColor: CustomColor.mainColor,
              )
            : AppBar(
                actions: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlatButton.icon(
                        hoverColor: Colors.grey[400],
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddCustomer(
                                        tableTitle,
                                        value: widget.value,
                                      )));
                        },
                        icon: Icon(
                          Icons.add,
                          color: CustomColor.drawerColor,
                        ),
                        label: Text(
                          "Add",
                          style: TextStyle(
                              color: CustomColor.drawerColor,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        )),
                  )
                ],
                leading: widget.icon,
                backgroundColor: CustomColor.mainColor,
                title: Text(
                  "All Customers",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
              ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Padding(
            padding: const EdgeInsets.all(25),
            child: BidirectionalScrollViewPlugin(
              scrollOverflow: Overflow.clip,
              child: Container(
                width: widget.value ? 1800 * widget.width : 1300,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Table(
                    border: TableBorder(
                        horizontalInside: BorderSide(
                            width: 1,
                            color: CustomColor.mainColor,
                            style: BorderStyle.solid)),
                    children: [
                          TableRow(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 2,
                                          color: CustomColor.textColor))),
                              children: tableTitle.map((e) {
                                var index = tableTitle.indexOf(e);
                                return getTableTitle(index);
                              }).toList())
                        ] +
                        customer.map((element) {
                          // get index
                          var index = customer.indexOf(element);
                          return getTableChildren(index);
                        }).toList(),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
