import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:web_project/colors/colors.dart';

class EditCustomers extends StatefulWidget {
  final bool value;
  final List tableTitle;
  EditCustomers(this.tableTitle,{ this.value=false});

  @override
  _EditCustomersState createState() => _EditCustomersState();
}

class _EditCustomersState extends State<EditCustomers> {
  Widget getTableTitle(int index, double width) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              widget.tableTitle[index],
              style: TextStyle(
                  color: CustomColor.drawerColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: width,
              child: TextField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: CustomColor.mainColor),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: CustomColor.mainColor),
                  ),
                  hintText: widget.tableTitle[index],
                  hintStyle:
                      TextStyle(color: CustomColor.drawerColor, fontSize: 15),
                ),
              ),
            ),
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: CustomColor.drawerColor,
              size: 30,
            ),
          ),
          backgroundColor: CustomColor.mainColor,
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 20),
          child: Center(
            child: Container(
              padding: EdgeInsets.all(20),

              width: MediaQuery.of(context).size.width / 1.5,

              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.5),

                      // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  border: Border.all(color: CustomColor.mainColor, width: 2)),
              child: Column(children: [
                Text(
                  "Edit Customer",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
                Divider(
                  thickness: 2,
                  color: CustomColor.textColor,
                ),
                ScreenTypeLayout(
                  desktop: Column(
                    children: [
                      Row(
                        children: [
                          getTableTitle(
                              0, MediaQuery.of(context).size.width / 3.5),
                          getTableTitle(
                              1, MediaQuery.of(context).size.width / 3.5),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              2, MediaQuery.of(context).size.width / 3.5),
                          getTableTitle(
                              3, MediaQuery.of(context).size.width / 3.5),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              4, MediaQuery.of(context).size.width / 3.5),
                          getTableTitle(
                              5, MediaQuery.of(context).size.width / 3.5),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              6, MediaQuery.of(context).size.width / 3.5),
                          getTableTitle(
                              7, MediaQuery.of(context).size.width / 3.5),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              8, MediaQuery.of(context).size.width / 3.5),
                        ],
                      ),
                    ],
                  ),
                  mobile: Column(
                    children: [
                      getTableTitle(0, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(1, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(2, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(3, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(4, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(5, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(6, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(7, MediaQuery.of(context).size.width / 2.3),
                      getTableTitle(8, MediaQuery.of(context).size.width / 2.3),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 200,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    onPressed: () {},
                    color: CustomColor.textColor,
                    elevation: 8,
                    child: Text(
                      "Edit",
                      style: TextStyle(
                          color: CustomColor.drawerColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 20),
                    ),
                  ),
                ),
              ]),
            ),
          ),
        )));
  }
}
