import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:web_project/colors/colors.dart';

class ViewCustomer extends StatefulWidget {
  @override
  _ViewCustomerState createState() => _ViewCustomerState();
}

class _ViewCustomerState extends State<ViewCustomer> {
  final List<Map> customerInfo = [
    {"Customer Id": "1"},
    {"Name": "Farah"},
    {"Number": "0302762854"},
    {"Details": "Student"},
    {"Address": "Shah Faisal"},
    {"Information": "Mobile App Developer"},
    {"Job": "XYZ"},
    {"salary": "1000"},
    {"lastName": "Yaqoob"},
    {"Hours": "7 hours"},
    {"Age": "20"},
    {"Gender": "Female"},
    {"Status": "1"}
  ];
  Widget getCustomerInfo(int index, String key, double width, String text) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: CustomColor.textColor,width: 2))
            ),
            child: Text(
              text,
              style: TextStyle(
                  // decoration: TextDecoration.underline,
                  // decorationColor: CustomColor.textColor,
                  color: CustomColor.drawerColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 17),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            customerInfo[index][key],
            style: TextStyle(color: CustomColor.drawerColor, fontSize: 17),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: CustomColor.drawerColor,
              size: 30,
            ),
          ),
          backgroundColor: CustomColor.mainColor,
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 20),
          child: Center(
            child: Container(
              padding: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.width / 1.5,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.5),

                      // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  border: Border.all(color: CustomColor.mainColor, width: 2)),
              child: Column(children: [
                Text(
                  "View Customer",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
                Divider(
                  thickness: 2,
                  color: CustomColor.textColor,
                ),
                ScreenTypeLayout(
                  desktop: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(
                                0,
                                "Customer Id",
                                MediaQuery.of(context).size.width / 3.5,
                                "Customer Id"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                1,
                                "Name",
                                MediaQuery.of(context).size.width / 3.5,
                                "Name"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(
                                2,
                                "Number",
                                MediaQuery.of(context).size.width / 3.5,
                                "Number"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                3,
                                "Details",
                                MediaQuery.of(context).size.width / 3.5,
                                "Details"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(
                                4,
                                "Address",
                                MediaQuery.of(context).size.width / 3.5,
                                "Address"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                5,
                                "Information",
                                MediaQuery.of(context).size.width / 3.5,
                                "Information"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(6, "Job",
                                MediaQuery.of(context).size.width / 3.5, "Job"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                7,
                                "salary",
                                MediaQuery.of(context).size.width / 3.5,
                                "salary"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(
                                8,
                                "lastName",
                                MediaQuery.of(context).size.width / 3.5,
                                "lastName"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                9,
                                "Hours",
                                MediaQuery.of(context).size.width / 3.5,
                                "Hours"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(10, "Age",
                                MediaQuery.of(context).size.width / 3.5, "Age"),
                          ),
                          Expanded(
                            child: getCustomerInfo(
                                11,
                                "Gender",
                                MediaQuery.of(context).size.width / 3.5,
                                "Gender"),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: getCustomerInfo(
                                12,
                                "Status",
                                MediaQuery.of(context).size.width / 3.5,
                                "Status"),
                          ),
                        ],
                      ),
                    ],
                  ),
                  mobile: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getCustomerInfo(
                          0,
                          "Customer Id",
                          MediaQuery.of(context).size.width / 2.3,
                          "Customer Id"),
                      getCustomerInfo(1, "Name",
                          MediaQuery.of(context).size.width / 2.3, "Name"),
                      getCustomerInfo(2, "Number",
                          MediaQuery.of(context).size.width / 2.3, "Number"),
                      getCustomerInfo(3, "Details",
                          MediaQuery.of(context).size.width / 2.3, "Details"),
                      getCustomerInfo(4, "Address",
                          MediaQuery.of(context).size.width / 2.3, "Address"),
                      getCustomerInfo(
                          5,
                          "Information",
                          MediaQuery.of(context).size.width / 2.3,
                          "Information"),
                      getCustomerInfo(6, "Job",
                          MediaQuery.of(context).size.width / 2.3, "Job"),
                      getCustomerInfo(7, "salary",
                          MediaQuery.of(context).size.width / 2.3, "salary"),
                      getCustomerInfo(8, "lastName",
                          MediaQuery.of(context).size.width / 2.3, "lastName"),
                      getCustomerInfo(9, "Hours",
                          MediaQuery.of(context).size.width / 2.3, "Hours"),
                      getCustomerInfo(10, "Age",
                          MediaQuery.of(context).size.width / 2.3, "Age"),
                      getCustomerInfo(11, "Gender",
                          MediaQuery.of(context).size.width / 2.3, "Gender"),
                      getCustomerInfo(12, "Status",
                          MediaQuery.of(context).size.width / 2.3, "Status"),
                    ],
                  ),
                ),
              ]),
            ),
          ),
        )));
  }
}
