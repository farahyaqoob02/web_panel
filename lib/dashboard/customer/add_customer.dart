import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:toast/toast.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/firebaseMethods/customer_service.dart';

class AddCustomer extends StatefulWidget {
  final bool value;
  final List tableTitle;
  AddCustomer(this.tableTitle, {this.value = false});
  @override
  _AddCustomerState createState() => _AddCustomerState();
}

class _AddCustomerState extends State<AddCustomer> {
  TextEditingController _idController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _detailController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _informationController = TextEditingController();
  TextEditingController _jobController = TextEditingController();
  TextEditingController _salaryController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Widget getTableTitle(
        int index, double width, TextEditingController _controller) {
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.tableTitle[index],
                style: TextStyle(
                    color: CustomColor.drawerColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: width,
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CustomColor.mainColor),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CustomColor.mainColor),
                    ),
                    hintText: widget.tableTitle[index],
                    hintStyle:
                        TextStyle(color: CustomColor.drawerColor, fontSize: 15),
                  ),
                ),
              ),
            ),
          ]);
    }

    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: CustomColor.drawerColor,
              size: 30,
            ),
          ),
          backgroundColor: CustomColor.mainColor,
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 20),
          child: Center(
            child: Container(
              padding: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.width / 1.5,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.5),

                      // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  border: Border.all(color: CustomColor.mainColor, width: 2)),
              child: Column(children: [
                Text(
                  "Add Customer",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
                Divider(
                  thickness: 2,
                  color: CustomColor.textColor,
                ),
                ScreenTypeLayout(
                  desktop: Column(
                    children: [
                      Row(
                        children: [
                          getTableTitle(
                              0,
                              MediaQuery.of(context).size.width / 3.5,
                              _idController),
                          getTableTitle(
                              1,
                              MediaQuery.of(context).size.width / 3.5,
                              _nameController),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              2,
                              MediaQuery.of(context).size.width / 3.5,
                              _numberController),
                          getTableTitle(
                              3,
                              MediaQuery.of(context).size.width / 3.5,
                              _detailController),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              4,
                              MediaQuery.of(context).size.width / 3.5,
                              _addressController),
                          getTableTitle(
                              5,
                              MediaQuery.of(context).size.width / 3.5,
                              _informationController),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              6,
                              MediaQuery.of(context).size.width / 3.5,
                              _jobController),
                          getTableTitle(
                              7,
                              MediaQuery.of(context).size.width / 3.5,
                              _salaryController),
                        ],
                      ),
                      Row(
                        children: [
                          getTableTitle(
                              8,
                              MediaQuery.of(context).size.width / 3.5,
                              _lastNameController),
                        ],
                      ),
                    ],
                  ),
                  mobile: Column(
                    children: [
                      getTableTitle(0, MediaQuery.of(context).size.width / 2.3,
                          _idController),
                      getTableTitle(1, MediaQuery.of(context).size.width / 2.3,
                          _nameController),
                      getTableTitle(2, MediaQuery.of(context).size.width / 2.3,
                          _numberController),
                      getTableTitle(3, MediaQuery.of(context).size.width / 2.3,
                          _detailController),
                      getTableTitle(4, MediaQuery.of(context).size.width / 2.3,
                          _addressController),
                      getTableTitle(5, MediaQuery.of(context).size.width / 2.3,
                          _informationController),
                      getTableTitle(6, MediaQuery.of(context).size.width / 2.3,
                          _jobController),
                      getTableTitle(7, MediaQuery.of(context).size.width / 2.3,
                          _salaryController),
                      getTableTitle(8, MediaQuery.of(context).size.width / 2.3,
                          _lastNameController),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 200,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    onPressed: () {
                      CustomerService().getAllCustomer();
                      // CustomerService().addCustomer(
                      //     _idController.text,
                      //     _nameController.text,
                      //     _numberController.text,
                      //     _detailController.text,
                      //     _addressController.text,
                      //     _informationController.text,
                      //     _jobController.text,
                      //     _salaryController.text,
                      //     _lastNameController.text);
                      Toast.show("New customer added", context,
                          duration: Toast.LENGTH_SHORT,
                          gravity: Toast.BOTTOM,
                          backgroundColor: CustomColor.drawerColor,
                          textColor: CustomColor.mainColor);

                      // Navigator.pop(context);
                    },
                    color: CustomColor.textColor,
                    elevation: 8,
                    child: Text(
                      "Add",
                      style: TextStyle(
                          color: CustomColor.drawerColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 20),
                    ),
                  ),
                ),
              ]),
            ),
          ),
        )));
  }
}
