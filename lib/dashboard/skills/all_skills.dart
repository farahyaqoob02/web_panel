import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class Skills extends StatefulWidget {
  final icon;
  final double width;
  final bool value;
  Skills({this.icon = '', this.width = 0, this.value = false});
  @override
  _SkillsState createState() => _SkillsState();
}

class _SkillsState extends State<Skills> {
  List skills = [
    "Painters",
    "Electrician",
    "Mechanics",
    "Technician",
    "Plumber",
    "Barber",
    "Carpenter",
    "Mechanics",
    "Technician",
    "Plumber",
    "Barber",
    "Carpenter"
  ];
  // List tableTitle = [
  //   "Skills",
  //   "Actions",
  // ];

  @override
  Widget build(BuildContext context) {
    // TableCell getTableTitle(int index) {
    //   return TableCell(
    //     child: Padding(
    //       padding: const EdgeInsets.all(8.0),
    //       child: Text(
    //         tableTitle[index],
    //         style: TextStyle(
    //             color: CustomColor.drawerColor,
    //             fontWeight: FontWeight.bold,
    //             fontSize: 17),
    //       ),
    //     ),
    //   );
    // }

    // showAlertDialog(BuildContext context, String customerKey) {
    //   // set up the buttons
    //   Widget cancelButton = FlatButton(
    //     child: Text("Cancel"),
    //     onPressed: () {
    //       Navigator.pop(context);
    //     },
    //   );
    //   Widget continueButton = FlatButton(
    //       child: Text(
    //         "Delete",
    //         style: TextStyle(color: Colors.red),
    //       ),
    //       onPressed: () {
    //         for (int i = 0; i <= customer.length; i++) {
    //           if (customer[i]["id"] == customerKey) {
    //             customer.removeAt(i);

    //             setState(() {
    //               customer = customer;
    //             });
    //             break;
    //           }
    //         }
    //         Navigator.pop(context);
    //       });

    //   // set up the AlertDialog
    //   AlertDialog alert = AlertDialog(
    //     title: Text("Delete Customer",
    //         style: TextStyle(color: CustomColor.drawerColor)),
    //     content: Text("Are you sure, you want to delete this customer?"),
    //     actions: [
    //       cancelButton,
    //       continueButton,
    //     ],
    //   );

    //   // show the dialog
    //   showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return alert;
    //     },
    //   );
    // }

    // TableRow getTableChildren(int index) {
    //   return TableRow(children: [
    //     TableCell(
    //       child: Padding(
    //         padding: EdgeInsets.symmetric(vertical: 8.0),
    //         child: Padding(
    //           padding: const EdgeInsets.only(left: 10),
    //           child: Text(
    //             skills[index],
    //             style: TextStyle(fontSize: 15),
    //           ),
    //         ),
    //       ),
    //     ),
    //     TableCell(
    //       child: Padding(
    //         padding: EdgeInsets.symmetric(vertical: 8.0),
    //         child: Row(
    //           children: [
    //             IconButton(
    //                 icon: Icon(
    //                   Icons.edit,
    //                   color: Colors.green[400],
    //                 ),
    //                 onPressed: () {
    //                   Navigator.push(
    //                       context,
    //                       MaterialPageRoute(
    //                           builder: (context) => EditCustomers(
    //                                 tableTitle,
    //                                 value: widget.value,
    //                               )));
    //                 }),
    //             IconButton(
    //                 icon: Icon(
    //                   Icons.delete,
    //                   color: Colors.red,
    //                 ),
    //                 onPressed: () {
    //                   setState(() {
    //                     // showAlertDialog(context, customer[index]["id"]);
    //                   });
    //                 }),
    //           ],
    //         ),
    //       ),
    //     ),
    //   ]);
    // }

    return Scaffold(
      backgroundColor: CustomColor.mainColor,
      appBar: widget.icon == ''
          ? AppBar(
              actions: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                      hoverColor: Colors.grey[400],
                      onPressed: () {},
                      icon: Icon(
                        Icons.add,
                        color: CustomColor.drawerColor,
                      ),
                      label: Text(
                        "Add",
                        style: TextStyle(
                            color: CustomColor.drawerColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )),
                )
              ],
              title: Text(
                "All Skills",
                style: TextStyle(
                    color: CustomColor.drawerColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
              backgroundColor: CustomColor.mainColor,
            )
          : AppBar(
              actions: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                      hoverColor: Colors.grey[400],
                      onPressed: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => AddCustomer(
                        //               tableTitle,
                        //               value: widget.value,
                        //             )));
                      },
                      icon: Icon(
                        Icons.add,
                        color: CustomColor.drawerColor,
                      ),
                      label: Text(
                        "Add",
                        style: TextStyle(
                            color: CustomColor.drawerColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      )),
                )
              ],
              leading: widget.icon,
              backgroundColor: CustomColor.mainColor,
              title: Text(
                "All Skills",
                style: TextStyle(
                    color: CustomColor.drawerColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
            itemCount: skills.length,
            itemBuilder: (context, index) {
              return Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5)),
                  width: widget.value ? 800 * widget.width : 500,
                  child: Center(child: Card(child: Text(skills[index]))));
            }),
        // child: ListView(
        //   children: [
        // Padding(
        //   padding: const EdgeInsets.all(15),
        //   child: Container(
        //     height: MediaQuery.of(context).size.height,
        //     decoration: BoxDecoration(
        //         color: Colors.white,
        //         borderRadius: BorderRadius.circular(5)),
        //     child: ListView.builder(
        //         shrinkWrap: true,
        //         itemCount: skills.length,
        //         itemBuilder: (context, index) {
        //           return Container(
        //               width: widget.value ? 800 * widget.width : 500,
        //               child: Center(
        //                   child: Card(child: Text(skills[index]))));
        //         }),
        //   ),
        // ),
        //  ],
        // ),
      ),

      // Row(
      //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //   children: [
      //     Padding(
      //       padding: const EdgeInsets.all(8.0),
      //       child: Container(
      //         width: widget.value ? 800 * widget.width : 500,
      //         decoration: BoxDecoration(
      //             color: Colors.white,
      //             borderRadius: BorderRadius.circular(5)),
      //         child: Padding(
      //           padding: const EdgeInsets.all(15),
      //           child: Table(
      //             border: TableBorder(
      //                 horizontalInside: BorderSide(
      //                     width: 1,
      //                     color: CustomColor.mainColor,
      //                     style: BorderStyle.solid)),
      //             children: [
      //                   TableRow(
      //                       decoration: BoxDecoration(
      //                           border: Border(
      //                               bottom: BorderSide(
      //                                   width: 2,
      //                                   color:
      //                                       CustomColor.textColor))),
      //                       children: tableTitle.map((e) {
      //                         var index = tableTitle.indexOf(e);
      //                         return getTableTitle(index);
      //                       }).toList())
      //                 ] +
      //                 skills.map((element) {
      //                   // get index
      //                   var index = skills.indexOf(element);
      //                   return getTableChildren(index);
      //                 }).toList(),
      //           ),
      //         ),
      //       ),
      //     ),
      //     Padding(
      //       padding: const EdgeInsets.all(8.0),
      //       child: Container(
      //         width: widget.value ? 800 * widget.width : 500,
      //         decoration: BoxDecoration(
      //             color: Colors.white,
      //             borderRadius: BorderRadius.circular(5)),
      //         child: Padding(
      //           padding: const EdgeInsets.all(8.0),
      //           child: Column(
      //             children: [
      //               Padding(
      //                 padding: const EdgeInsets.all(8.0),
      //                 child: Text(
      //                   "Enter a skill",
      //                   style: TextStyle(
      //                       color: CustomColor.drawerColor,
      //                       fontWeight: FontWeight.bold,
      //                       fontSize: 30),
      //                 ),
      //               ),
      //               Padding(
      //                 padding: const EdgeInsets.all(8.0),
      //                 child: TextField(
      //                   decoration: InputDecoration(
      //                     focusedBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(
      //                           color: CustomColor.mainColor),
      //                     ),
      //                     enabledBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(
      //                           color: CustomColor.mainColor),
      //                     ),
      //                     hintText: "Electrician",
      //                     hintStyle: TextStyle(
      //                         color: CustomColor.drawerColor,
      //                         fontSize: 15),
      //                   ),
      //                 ),
      //               ),
      //               Padding(
      //                 padding: const EdgeInsets.all(8.0),
      //                 child: Container(
      //                   width: 200,
      //                   height: 50,
      //                   child: RaisedButton(
      //                     shape: RoundedRectangleBorder(
      //                         borderRadius: BorderRadius.circular(5)),
      //                     onPressed: () {},
      //                     color: CustomColor.textColor,
      //                     elevation: 8,
      //                     child: Text(
      //                       "Add",
      //                       style: TextStyle(
      //                           color: CustomColor.drawerColor,
      //                           fontWeight: FontWeight.w700,
      //                           fontSize: 20),
      //                     ),
      //                   ),
      //                 ),
      //               ),
      //             ],
      //           ),
      //         ),
      //       ),
      //     )
      //   ],
      // ),

      // body: Padding(
      //   padding: const EdgeInsets.all(25),
      //   child: ListView(
      //     children: [
      //       Container(
      //         width: widget.value ? 800 * widget.width : 500,
      //         decoration: BoxDecoration(
      //             color: Colors.white,
      //             borderRadius: BorderRadius.circular(5)),
      //         child: Padding(
      //           padding: const EdgeInsets.all(15),
      //           child: Table(
      //             border: TableBorder(
      //                 horizontalInside: BorderSide(
      //                     width: 1,
      //                     color: CustomColor.mainColor,
      //                     style: BorderStyle.solid)),
      //             children: [
      //                   TableRow(
      //                       decoration: BoxDecoration(
      //                           border: Border(
      //                               bottom: BorderSide(
      //                                   width: 2,
      //                                   color: CustomColor.textColor))),
      //                       children: tableTitle.map((e) {
      //                         var index = tableTitle.indexOf(e);
      //                         return getTableTitle(index);
      //                       }).toList())
      //                 ] +
      //                 skills.map((element) {
      //                   // get index
      //                   var index = skills.indexOf(element);
      //                   return getTableChildren(index);
      //                 }).toList(),
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // )
    );
  }
}
