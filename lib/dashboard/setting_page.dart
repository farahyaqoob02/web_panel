import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
class Setting extends StatefulWidget {
   final icon;
  Setting({this.icon});
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         backgroundColor: CustomColor.mainColor,
      appBar:widget.icon==''?AppBar(
        
        title: Text(
          "Setting",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ): AppBar(
         leading: widget.icon,
          backgroundColor: CustomColor.mainColor,
        title: Text(
                  "Setting",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
      ),
          body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
         
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Setting",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}