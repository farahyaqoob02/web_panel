import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class ContactPage extends StatefulWidget {
   final  icon;
  ContactPage({this.icon});
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         backgroundColor: CustomColor.mainColor,
      appBar:widget.icon==''?AppBar(
        
        title: Text(
          "Contact Us",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ): AppBar(
         leading: widget.icon,
          backgroundColor: CustomColor.mainColor,
        title: Text(
                  "Contact Us",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ) ,
      ),
          body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
                                          
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Contact Us",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
