import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/dashboard/about_page.dart';
import 'package:web_project/dashboard/customer/all_customers.dart';
import 'package:web_project/dashboard/all_workers.dart';
import 'package:web_project/dashboard/approved_customers.dart';
import 'package:web_project/dashboard/approved_workers.dart';
import 'package:web_project/dashboard/contact_page.dart';
import 'package:web_project/dashboard/products_page.dart';
import 'package:web_project/dashboard/skills/all_skills.dart';
import 'package:web_project/dashboard/unapproved_customers.dart';
import 'package:web_project/dashboard/setting_page.dart';
import 'package:web_project/dashboard/unapproved_workers.dart';
import 'package:web_project/extras/expansion1.dart';
import 'package:web_project/extras/expansion2.dart';
import 'package:web_project/extras/shared_preferences.dart';

class DesktopDashboard extends StatefulWidget {
  double dashboardWidth;
  double drawerWidth;
  DesktopDashboard({this.dashboardWidth = 0, this.drawerWidth = 0});
  @override
  _DesktopDashboardState createState() => _DesktopDashboardState();
}

class _DesktopDashboardState extends State<DesktopDashboard> {
  String currentPage = "productPage";
  setPageType() async {
    await Preferences().getBodyType().then((value) {
      if (value == "allCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "allWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "aboutPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "contactPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "productPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "settingPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "skillsPage") {
        setState(() {
          currentPage = value;
        });
      }
    });
  }

  @override
  void initState() {
    setPageType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width:
                        MediaQuery.of(context).size.width * widget.drawerWidth,
                    color: CustomColor.drawerColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Image.asset(
                                  "images/logo.png",
                                  scale: 20,
                                  colorBlendMode: BlendMode.clear,
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Center(
                                child: Text(
                                  "Admin Panel",
                                  style: TextStyle(
                                      color: CustomColor.mainColor,
                                      fontSize: 21,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              Divider(
                                thickness: 1,
                              ),
                              CustomExpansionCard1(
                                () {
                                  Preferences().setBodyType("productPage");
                                  setState(() {
                                    currentPage = "productPage";
                                  });
                                },
                                () {
                                  Preferences().setBodyType("productPage");
                                  setState(() {
                                    currentPage = "productPage";
                                  });
                                },
                                icon: Icons.info,
                                text: "Products",
                                childText1: "Dashboard1",
                                childText2: "Dashboard2",
                              ),
                              CustomExpansionCard2(
                                () {
                                  Preferences().setBodyType("allCustomers");
                                  setState(() {
                                    currentPage = "allCustomers";
                                  });
                                },
                                () {
                                  Preferences()
                                      .setBodyType("approvedCustomers");
                                  setState(() {
                                    currentPage = "approvedCustomers";
                                  });
                                },
                                () {
                                  Preferences()
                                      .setBodyType("unapprovedCustomers");
                                  setState(() {
                                    currentPage = "unapprovedCustomers";
                                  });
                                },
                                icon: Icons.person,
                                text: "Customers",
                                childText1: "All",
                                childText2: "Approved",
                                childText3: "Non Approved",
                              ),
                              CustomExpansionCard2(
                                () {
                                  Preferences().setBodyType("allWorkers");
                                  setState(() {
                                    currentPage = "allWorkers";
                                  });
                                },
                                () {
                                  Preferences().setBodyType("approvedWorkers");
                                  setState(() {
                                    currentPage = "approvedWorkers";
                                  });
                                },
                                () {
                                  Preferences()
                                      .setBodyType("unapprovedWorkers");
                                  setState(() {
                                    currentPage = "unapprovedWorkers";
                                  });
                                },
                                icon: Icons.work_rounded,
                                text: "Workers",
                                childText1: "All",
                                childText2: "Approved",
                                childText3: "Non Approved",
                              ),
                              CustomExpansionCard1(
                                () {
                                  Preferences().setBodyType("settingPage");
                                  setState(() {
                                    currentPage = "settingPage";
                                  });
                                },
                                () {
                                  Preferences().setBodyType("settingPage");
                                  setState(() {
                                    currentPage = "settingPage";
                                  });
                                },
                                icon: Icons.settings,
                                text: "Settings",
                                childText1: "Dashboard1",
                                childText2: "Dashboard2",
                              ),
                              ExpansionCard(
                                trailing: Icon(
                                  Icons.arrow_back,
                                  color: CustomColor.drawerColor,
                                ),
                                margin: EdgeInsets.zero,
                                title: Row(
                                  children: [
                                    Icon(
                                      Icons.info_rounded,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Preferences().setBodyType("aboutPage");
                                        setState(() {
                                          currentPage = "aboutPage";
                                        });
                                      },
                                      child: Text(
                                        "About Us",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ExpansionCard(
                                trailing: Icon(
                                  Icons.arrow_back,
                                  color: CustomColor.drawerColor,
                                ),
                                margin: EdgeInsets.zero,
                                title: Row(
                                  children: [
                                    Icon(
                                      Icons.add,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Preferences()
                                            .setBodyType("skillsPage");
                                        setState(() {
                                          currentPage = "skillsPage";
                                        });
                                      },
                                      child: Text(
                                        "Skills",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ExpansionCard(
                                trailing: Icon(
                                  Icons.arrow_back,
                                  color: CustomColor.drawerColor,
                                ),
                                margin: EdgeInsets.zero,
                                title: Row(
                                  children: [
                                    Icon(
                                      Icons.call,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Preferences()
                                            .setBodyType("contactPage");
                                        setState(() {
                                          currentPage = "contactPage";
                                        });
                                      },
                                      child: Text(
                                        "Contact Us",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Divider(
                                  thickness: 1,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.logout,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Logout",
                                      style: TextStyle(
                                          color: CustomColor.mainColor,
                                          fontSize: 15),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width *
                          widget.dashboardWidth,
                      color: CustomColor.mainColor,
                      child: currentPage == "allCustomers"
                          ? AllCustomers(
                              value: true,
                              icon: '',
                              width: widget.dashboardWidth,
                            )
                          : currentPage == "settingPage"
                              ? Setting(
                                  icon: '',
                                )
                              : currentPage == "aboutPage"
                                  ? AboutPage(
                                      icon: '',
                                    )
                                  : currentPage == "contactPage"
                                      ? ContactPage(
                                          icon: '',
                                        )
                                      : currentPage == "productPage"
                                          ? ProductsPage(
                                              icon: '',
                                            )
                                          : currentPage == "approvedCustomers"
                                              ? ApprovedCustomers(
                                                  icon: '',
                                                )
                                              : currentPage ==
                                                      "unapprovedCustomers"
                                                  ? UnapprovedCustomers(
                                                      icon: '',
                                                    )
                                                  : currentPage == "allWorkers"
                                                      ? AllWorkers(
                                                          icon: '',
                                                        )
                                                      : currentPage ==
                                                              "approvedWorkers"
                                                          ? ApprovedWorkers(
                                                              icon: '',
                                                            )
                                                          : currentPage ==
                                                                  "unapprovedWorkers"
                                                              ? UnapprovedWorkers(
                                                                  icon: '',
                                                                )
                                                              : currentPage ==
                                                                      "skillsPage"
                                                                  ? Skills(
                                                                    icon: '',
                                                                  )
                                                                  : ProductsPage(
                                                                      icon: '',
                                                                    )),
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
