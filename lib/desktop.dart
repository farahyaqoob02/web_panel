import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:web_project/screens/login.dart';

class Desktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      desktop: LogIn(
       MediaQuery.of(context).size.width / 2.8,
       9,
      ),
      mobile: LogIn(
       MediaQuery.of(context).size.width,
         15,
      ),
      tablet: LogIn(
         MediaQuery.of(context).size.width / 1.5,
         12,
      ),
    );
  }
}
