import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/drawer/desktop_drawer.dart';

import '../drawer/mobile_drawer.dart';

class LogIn extends StatefulWidget {
  final double width;
  final double scale;

  LogIn(this.width, this.scale);
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> with SingleTickerProviderStateMixin{
  Animation animation;
  AnimationController animationController;
  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    animation = Tween(begin: -1.0, end: 0.0).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    animationController.forward();
  }
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        body: AnimatedBuilder(
          animation: animationController,
          builder: (BuildContext context, Widget child) {
            return Transform(
              transform:
                  Matrix4.translationValues(0.0, animation.value * width, 0.0),
                          child: Container(
                alignment: Alignment.center,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Image.asset(
                            "images/logo.png",
                            scale: widget.scale,
                            colorBlendMode: BlendMode.clear,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          //  height: widget.height,
                          width: widget.width,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: CustomColor.drawerColor.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                              borderRadius: BorderRadius.circular(20),
                              color: CustomColor.drawerColor,
                              border: Border.all(
                                  color: CustomColor.drawerColor, width: 2)),
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        "Login",
                                        style: TextStyle(
                                            color: CustomColor.textColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 35),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        "Sign in to your admin panel",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 18),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: CustomColor.textColor),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: CustomColor.textColor),
                                          ),
                                          hintText: "Email",
                                          prefixIcon: Icon(
                                            Icons.email,
                                            color: CustomColor.mainColor,
                                            size: 20,
                                          ),
                                          hintStyle: TextStyle(
                                              color: CustomColor.textColor,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                        obscureText: true,
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: CustomColor.textColor),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: CustomColor.textColor),
                                          ),
                                          hintText: "Passward",
                                          prefixIcon: Icon(
                                            Icons.lock,
                                            color: CustomColor.mainColor,
                                            size: 20,
                                          ),
                                          hintStyle: TextStyle(
                                              color: CustomColor.textColor,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            height: 50,
                                            width: widget.width / 3.5,
                                            child: RaisedButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              onPressed: () {
                                                Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ScreenTypeLayout(
                                                              mobile:
                                                                  MobileDashboard(),
                                                              tablet:
                                                                  MobileDashboard(),
                                                              desktop:
                                                                  DesktopDashboard(
                                                                dashboardWidth: 0.81,
                                                                drawerWidth: 0.19,
                                                              ),
                                                            )));
                                              },
                                              color: CustomColor.textColor,
                                              elevation: 8,
                                              child: Text(
                                                "Login",
                                                style: TextStyle(
                                                    color: CustomColor.drawerColor,
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 18),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8),
                                          child: Text(
                                            "Forget password?",
                                            style: TextStyle(
                                                color: CustomColor.mainColor,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Image.asset(
                            "images/logo.png",
                            scale: widget.scale * 2,
                            color: CustomColor.mainColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        ));
  }
}
